#!/bin/bash

# Let's run cluquebait from the base image
cd /cliquebait && ./run.bash &

cd /app
# Call initialize on predeployed contracts to set storage
npx hardhat run --network cliquebait scripts/initialize_predeployed.ts

# Let this container stay running
sleep infinity
