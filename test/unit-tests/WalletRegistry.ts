// Copyright © 2022 TXA PTE. LTD.
import {WalletRegistry} from "../../typechain/WalletRegistry"
import {WalletRegistry__factory} from "../../typechain/factories/WalletRegistry__factory"
import {ethers, getNamedAccounts} from "hardhat"
import {expect} from "chai"

describe("Wallet Registry", function () {
    let walletRegistryFactory: WalletRegistry__factory
    let walletRegistry: WalletRegistry
    let alice: string
    const sig = {
        v: 1,
        r: ethers.utils.hexlify(ethers.utils.randomBytes(32)),
        s: ethers.utils.hexlify(ethers.utils.randomBytes(32)),
        publicKey: ethers.utils.hexlify(ethers.utils.randomBytes(64)),
    }

    before(async () => {
        walletRegistryFactory = (await ethers.getContractFactory("WalletRegistry")) as WalletRegistry__factory
        alice = (await getNamedAccounts()).alice
    })

    beforeEach(async () => {
        walletRegistry = await walletRegistryFactory.deploy()
    })

    it("allows trader to set a signature", async () => {
        const sigBefore = await walletRegistry.callStatic.signatures(alice)
        expect(sigBefore.v).to.be.eq(0)
        expect(sigBefore.r).to.be.eq(ethers.constants.HashZero)
        expect(sigBefore.s).to.be.eq(ethers.constants.HashZero)
        expect(sigBefore.publicKey).to.be.eq("0x")

        await walletRegistry
            .connect(await ethers.getSigner(alice))
            .approveSignature(sig.v, sig.r, sig.s, sig.publicKey)

        const sigAfter = await walletRegistry.callStatic.signatures(alice)
        expect(sigAfter.v).to.be.eq(sig.v)
        expect(sigAfter.r).to.be.eq(sig.r)
        expect(sigAfter.s).to.be.eq(sig.s)
        expect(sigAfter.publicKey).to.be.eq(sig.publicKey)
    })

    it("emits an event when approving a signature", async () => {
        await expect(
            walletRegistry
                .connect(await ethers.getSigner(alice))
                .approveSignature(sig.v, sig.r, sig.s, sig.publicKey)
        )
            .to.emit(walletRegistry, "KeyApproved")
            .withArgs(alice, sig.v, sig.r, sig.s, sig.publicKey, 0)
    })

    it("forbids a trader from setting a signature where each value is 0", async () => {
        const sig = {
            v: 0,
            r: ethers.constants.HashZero,
            s: ethers.constants.HashZero,
            publicKey: "0x",
        }
        await expect(
            walletRegistry
                .connect(await ethers.getSigner(alice))
                .approveSignature(sig.v, sig.r, sig.s, sig.publicKey)
        ).to.be.revertedWith("INVALID_SIGNATURE")
    })

    it("forbids a trader from revoking a signature when none is set", async () => {
        await expect(
            walletRegistry.connect(await ethers.getSigner(alice)).revokeSignature()
        ).to.be.revertedWith("SIGNATURE_NOT_SET")
    })

    describe("after setting a key", async () => {
        beforeEach(async () => {
            await walletRegistry
                .connect(await ethers.getSigner(alice))
                .approveSignature(sig.v, sig.r, sig.s, sig.publicKey)
        })

        it("forbids a trader from setting a signature when an association already exists", async () => {
            await expect(
                walletRegistry
                    .connect(await ethers.getSigner(alice))
                    .approveSignature(sig.v, sig.r, sig.s, sig.publicKey)
            ).to.be.revertedWith("SIGNATURE_ALREADY_SET")
        })

        it("allows a trader to revoke a signature", async () => {
            await walletRegistry.connect(await ethers.getSigner(alice)).revokeSignature()
            const sigAfter = await walletRegistry.callStatic.signatures(alice)
            expect(sigAfter.v).to.be.eq(0)
            expect(sigAfter.r).to.be.eq(ethers.constants.HashZero)
            expect(sigAfter.s).to.be.eq(ethers.constants.HashZero)
        })

        it("emits an event when revoking a signature", async () => {
            await expect(walletRegistry.connect(await ethers.getSigner(alice)).revokeSignature())
                .to.emit(walletRegistry, "KeyRevoked")
                .withArgs(alice, sig.v, sig.r, sig.s, sig.publicKey)
        })
    })
})
