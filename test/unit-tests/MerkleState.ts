// Copyright © 2022 TXA PTE. LTD.
import {MerkleState} from "../../typechain/MerkleState"
import {MerkleState__factory} from "../../typechain/factories/MerkleState__factory"
import {ethers, getNamedAccounts} from "hardhat"
import {Address} from "hardhat-deploy/dist/types"
import {Signer, BigNumber} from "ethers"
import {expect} from "chai"
import {operatorInit, CollateralMock, setupTest, IdentityRegistry} from "../utils"

describe("Merkle State", function () {
    let merkleStateFactory: MerkleState__factory
    let merkleState: MerkleState
    let identity: IdentityRegistry
    const sig = {
        v: 1,
        r: ethers.utils.hexlify(ethers.utils.randomBytes(32)),
        s: ethers.utils.hexlify(ethers.utils.randomBytes(32)),
        publicKey: ethers.utils.hexlify(ethers.utils.randomBytes(64)),
    }

    let sdpAdmin1: Signer
    let sdpAdmin2: Signer
    let sdpAdmin3: Signer
    let sdpAdmin4: Signer
    let sdpOperator1: Signer
    let sdpOperator2: Signer
    let sdpOperator3: Signer
    let sdpOperator4: Signer

    let alice: Signer
    let aliceAddr: string

    const merkleRootZero = "0x0000000000000000000000000000000000000000000000000000000000000000"
    const merkleRootOne = "0x0000000000000000000000000000000000000000000000000000000000000001"
    const merkleRootTwo = "0x0000000000000000000000000000000000000000000000000000000000000002"
    const merkleRootThree = "0x0000000000000000000000000000000000000000000000000000000000000003"

    let collateralCustodyMock: CollateralMock

    before(async () => {
        const namedAccounts = await getNamedAccounts()
        merkleStateFactory = (await ethers.getContractFactory("MerkleState")) as MerkleState__factory
        aliceAddr = (await getNamedAccounts()).alice
        alice = await ethers.getSigner(aliceAddr)

        sdpAdmin1 = await ethers.provider.getSigner(namedAccounts.sdpAdmin1)
        sdpAdmin2 = await ethers.provider.getSigner(namedAccounts.sdpAdmin2)
        sdpAdmin3 = await ethers.provider.getSigner(namedAccounts.sdpAdmin3)
        sdpAdmin4 = await ethers.provider.getSigner(namedAccounts.sdpAdmin4)
        sdpOperator1 = await ethers.provider.getSigner(namedAccounts.sdpOperator1)
        sdpOperator2 = await ethers.provider.getSigner(namedAccounts.sdpOperator2)
        sdpOperator3 = await ethers.provider.getSigner(namedAccounts.sdpOperator3)
        sdpOperator4 = await ethers.provider.getSigner(namedAccounts.sdpOperator4)
    })

    beforeEach(async () => {
        let contracts = await setupTest()
        identity = contracts.identity
        merkleState = await merkleStateFactory.deploy(identity.address)
        collateralCustodyMock = contracts.collateralMock

        await identity.updateRole(
            await contracts.library.callStatic.ROLE_COLLATERAL_CUSTODY(),
            await collateralCustodyMock.address
        )
    })

    describe("testing basic trade merkle root submission", async () => {
        beforeEach(async () => {
            await operatorInit(sdpAdmin1, sdpOperator1, collateralCustodyMock)
            await operatorInit(sdpAdmin2, sdpOperator2, collateralCustodyMock)
            await operatorInit(sdpAdmin3, sdpOperator3, collateralCustodyMock)
        })
        it("SDP operator can submit merkle root", async () => {
            await merkleState.connect(sdpOperator1).reportTradeRoot(merkleRootOne)
            const merkleState1 = await merkleState.callStatic.tradeRoots(0)
            expect(merkleState1).to.be.eq(merkleRootOne)
        })
        it("Submitting trade roots increments trade root block ID", async () => {
            for (let ii = 0; ii < 20; ii++) {
                expect(await merkleState.callStatic.tradeRootID()).to.be.eq(ii)
                await merkleState.connect(sdpOperator1).reportTradeRoot(merkleRootZero)
            }
        })
        it("Multiple Operators can submit many merkle roots, trade Ids kept in sync", async () => {
            await merkleState.connect(sdpOperator1).reportTradeRoot(merkleRootOne)
            const merkleState1 = await merkleState.callStatic.tradeRoots(0)
            expect(merkleState1).to.be.eq(merkleRootOne)
            await merkleState.connect(sdpOperator2).reportTradeRoot(merkleRootTwo)
            const merkleState2 = await merkleState.callStatic.tradeRoots(1)
            expect(merkleState2).to.be.eq(merkleRootTwo)
            await merkleState.connect(sdpOperator3).reportTradeRoot(merkleRootThree)
            const merkleState3 = await merkleState.callStatic.tradeRoots(2)
            expect(merkleState3).to.be.eq(merkleRootThree)
        })
        it("non SDP Operators cannot report merkle roots", async () => {
            await expect(merkleState.connect(alice).reportTradeRoot(merkleRootZero)).to.be.revertedWith(
                "NOT_APPROVED_OPERATOR"
            )
        })
    })
})
