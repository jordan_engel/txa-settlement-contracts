// Copyright © 2022 TXA PTE. LTD.
import {expect} from "chai"
import {ethers, getNamedAccounts, config, deployments, getUnnamedAccounts} from "hardhat"
import {Address} from "hardhat-deploy/dist/types"

import {getDeployed, setupTest, ExchangeEIP712, _za} from "../utils"

describe("ExchangeEIP712", function () {
    const version = "1"
    let verifierInstance: ExchangeEIP712
    let chainId: any
    let exchange: Address

    before(async () => {
        const accounts = await getNamedAccounts()
        exchange = accounts.exchange
        chainId = await ethers.provider.send("eth_chainId", [])
    })

    beforeEach(async () => {
        await setupTest()
        verifierInstance = (await getDeployed("ExchangeEIP712")) as ExchangeEIP712
    })

    it("should verify that an Order was signed by an address", async () => {
        const verifierAddress = verifierInstance.address
        const verifierSalt = await verifierInstance.callStatic.salt()

        const msgParams = {
            types: {
                EIP712Domain: [
                    {name: "name", type: "string"},
                    {name: "version", type: "string"},
                    {name: "chainId", type: "uint256"},
                    {name: "verifyingContract", type: "address"},
                    {name: "salt", type: "bytes32"},
                ],
                Order: [
                    {name: "identity", type: "Identity"},
                    {name: "product", type: "string"},
                    {name: "side", type: "string"},
                    {name: "orderType", type: "string"},
                    {name: "fillType", type: "string"},
                    {name: "size", type: "string"},
                    {name: "price", type: "string"},
                ],
                Identity: [
                    {name: "baseAssetEscrow", type: "string"},
                    {name: "baseAssetNetworkType", type: "string"},
                    {name: "baseAssetChainId", type: "string"},
                    {name: "counterAssetEscrow", type: "string"},
                    {name: "counterAssetNetworkType", type: "string"},
                    {name: "counterAssetChainId", type: "string"},
                ],
            },
            primaryType: "Order",
            domain: {
                name: "TXA",
                version: version,
                chainId: chainId,
                verifyingContract: verifierAddress,
                salt: verifierSalt,
            },
            message: {
                identity: {
                    baseAssetEscrow: `${verifierAddress}`,
                    baseAssetNetworkType: "ETH",
                    baseAssetChainId: "1",
                    counterAssetEscrow: "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa",
                    counterAssetNetworkType: "BTC",
                    counterAssetChainId: "1",
                },
                product: "BTC/USDT",
                side: "BUY",
                orderType: "LIMIT",
                fillType: "NORM",
                size: "10.00000000",
                price: "9100.00",
            },
        }

        const from = exchange
        const params = [from, msgParams]
        const method = "eth_signTypedData_v4"
        const signedData = await ethers.provider.send(method, params)

        const signature = signedData.substring(2)
        const r = "0x" + signature.substring(0, 64)
        const s = "0x" + signature.substring(64, 128)
        const v = parseInt(signature.substring(128, 130), 16)

        const sigMatches = await verifierInstance.verify(from, msgParams.message, v, r, s)

        expect(sigMatches).to.be.true
    })
})
