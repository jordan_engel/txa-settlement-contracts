// Copyright © 2022 TXA PTE. LTD.
import {expect} from "chai"
import {ethers, getNamedAccounts, getUnnamedAccounts} from "hardhat"
import {Address} from "hardhat-deploy/dist/types"
import {Signer, BigNumber} from "ethers"

import {MerkleTree} from "merkletreejs"
import {
    copyArray,
    deploy,
    getSigner,
    IdentityRegistry,
    Obligation,
    CollateralTransfer,
    TransferObligation,
    ExchangeReport,
    DummyCoin,
    SettlementCoordination,
    SettlementDataConsensus,
    ConsensusMock,
    AssetMock,
    CollateralMock,
    CoordinationMock,
    setupTest,
    _za,
    approveAndDeposit,
    operatorInit,
    merkleFromObs,
} from "../utils"

describe("SettlementUtilities", function () {
    let coordination: CoordinationMock
    let assetCustodyMock: AssetMock
    let collateralCustodyMock: CollateralMock
    let identity: IdentityRegistry
    let noRole: Address

    let alice: Signer
    let bob: Signer
    let charlie: Signer
    let sdps: Signer[] = []
    let auditor: Signer
    let exchange: Signer

    let sdpAdmin1: Signer
    let sdpAdmin2: Signer
    let sdpAdmin3: Signer
    let sdpAdmin4: Signer

    let aliceAddr: string
    let bobAddr: string
    let charlieAddr: string

    let sdpAdmin1Address: Address
    let sdpAdmin2Address: Address
    let sdpAdmin3Address: Address
    let sdpAdmin4Address: Address

    let auditorAddress: Address

    let dMCAddress: Address

    let unnamedAccounts: Address[]
    let consensus: ConsensusMock
    let dummyCoin: DummyCoin

    let assetCustodyAddress: Address
    let collateralCustodyAddress: Address
    const zeroEth = ethers.utils.parseEther("0")
    const ethObligation = ethers.utils.parseEther("1")

    const merkleRootZero = "0x0000000000000000000000000000000000000000000000000000000000000000"
    let fakeMerkleRoot = "0x0000000000000000000000000000000000000000000000000000000000000000"
    let ethersMerkleRoot = "0x0000000000000000000000000000000000000000000000000000000000000000"

    let auditorFraction: number

    let bigObligations: Obligation[]
    let fakeObligations: Obligation[]

    before(async () => {
        const namedAccounts = await getNamedAccounts()
        unnamedAccounts = await getUnnamedAccounts()
        alice = await getSigner(namedAccounts.alice)
        bob = await getSigner(namedAccounts.bob)
        auditor = await getSigner(namedAccounts.auditor)
        exchange = await getSigner(namedAccounts.exchange)
        charlie = await getSigner(namedAccounts.charlie)
        sdps.push(
            await getSigner(namedAccounts.sdpOperator1),
            await getSigner(namedAccounts.sdpOperator2),
            await getSigner(namedAccounts.sdpOperator3)
        )

        sdpAdmin1 = await ethers.provider.getSigner(namedAccounts.sdpAdmin1)
        sdpAdmin2 = await ethers.provider.getSigner(namedAccounts.sdpAdmin2)
        sdpAdmin3 = await ethers.provider.getSigner(namedAccounts.sdpAdmin3)
        sdpAdmin4 = await ethers.provider.getSigner(namedAccounts.sdpAdmin4)

        sdpAdmin1Address = await sdpAdmin1.getAddress()
        sdpAdmin2Address = await sdpAdmin2.getAddress()
        sdpAdmin3Address = await sdpAdmin3.getAddress()
        sdpAdmin4Address = await sdpAdmin4.getAddress()
        auditorAddress = await auditor.getAddress()

        noRole = namedAccounts.noRole
        aliceAddr = await alice.getAddress()
        bobAddr = await bob.getAddress()
        charlieAddr = await charlie.getAddress()

        auditorFraction = 0.1
        // very large obligation array for testing
    })

    beforeEach(async function () {
        let contracts = await setupTest()
        identity = contracts.identity
        coordination = contracts.coordinationMock
        assetCustodyMock = contracts.assetMock
        collateralCustodyMock = contracts.collateralMock
        consensus = contracts.consensusMock

        dummyCoin = contracts.dummyCoin
        dMCAddress = dummyCoin.address

        assetCustodyAddress = assetCustodyMock.address
        collateralCustodyAddress = collateralCustodyMock.address

        await identity.updateRole(await contracts.library.callStatic.ROLE_CONSENSUS(), consensus.address)
        await identity.updateRole(
            await contracts.library.callStatic.ROLE_ASSET_CUSTODY(),
            await assetCustodyAddress
        )
        await identity.updateRole(
            await contracts.library.callStatic.ROLE_COLLATERAL_CUSTODY(),
            await collateralCustodyAddress
        )

        bigObligations = [
            {
                amount: 1000,
                recipient: aliceAddr,
                deliverer: bobAddr,
                token: dMCAddress,
                reallocate: false,
            },
            {
                amount: 2000,
                recipient: bobAddr,
                deliverer: charlieAddr,
                token: dMCAddress,
                reallocate: false,
            },
            {
                amount: 2000,
                recipient: sdpAdmin1Address,
                deliverer: charlieAddr,
                token: dMCAddress,
                reallocate: false,
            },
            {
                amount: 1000,
                recipient: bobAddr,
                deliverer: aliceAddr,
                token: dMCAddress,
                reallocate: false,
            },
            {
                amount: 2000,
                recipient: charlieAddr,
                deliverer: aliceAddr,
                token: dMCAddress,
                reallocate: false,
            },
            {
                amount: 1000,
                recipient: bobAddr,
                deliverer: aliceAddr,
                token: dMCAddress,
                reallocate: false,
            },
            {
                amount: 2000,
                recipient: bobAddr,
                deliverer: sdpAdmin1Address,
                token: dMCAddress,
                reallocate: false,
            },
            {
                amount: 3000,
                recipient: charlieAddr,
                deliverer: sdpAdmin1Address,
                token: dMCAddress,
                reallocate: false,
            },
            {
                amount: 4000,
                recipient: bobAddr,
                deliverer: sdpAdmin1Address,
                token: dMCAddress,
                reallocate: false,
            },
            {
                amount: 2000,
                recipient: bobAddr,
                deliverer: sdpAdmin1Address,
                token: dMCAddress,
                reallocate: false,
            },
            {
                amount: 1000,
                recipient: charlieAddr,
                deliverer: sdpAdmin1Address,
                token: dMCAddress,
                reallocate: false,
            },
        ]
    })
    it("compares obligations correctly", async () => {
        const testCompare = async (a: Obligation[], b: Obligation[], match: boolean = true) => {
            expect(await consensus.callStatic.compareObligations(a, b)).to.be.eq(match)
        }

        const a: Obligation[] = [
            {
                amount: ethObligation,
                recipient: aliceAddr,
                deliverer: bobAddr,
                token: ethers.constants.AddressZero,
                reallocate: false,
            },
            {
                amount: ethObligation,
                recipient: bobAddr,
                deliverer: aliceAddr,
                token: noRole,
                reallocate: false,
            },
        ]
        let b: Obligation[] = copyArray(a)

        await testCompare(a, b)

        b[1].token = ethers.constants.AddressZero

        await testCompare(a, b, false)

        b = copyArray(a)
        b[1].amount = ethers.utils.parseEther("0.99")

        await testCompare(a, b, false)

        b = [a[1], a[0]]

        await testCompare(a, b, false)

        b = copyArray(a)
        b.push({
            amount: ethers.utils.parseEther("0.5"),
            recipient: aliceAddr,
            deliverer: bobAddr,
            token: noRole,
            reallocate: false,
        })

        await testCompare(a, b, false)
    })
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    describe("Check Merkle Root Generation Function is Working", async () => {
        it("Verifies Correct Merkle Root Generated for Arbitrary large sized Leaf Array", async () => {
            const calculateLeafFromObligation = (obligation: Obligation) =>
                ethers.utils.solidityKeccak256(
                    ["uint256", "address", "address", "address", "bool"],
                    [
                        obligation.amount,
                        obligation.deliverer,
                        obligation.recipient,
                        obligation.token,
                        obligation.reallocate,
                    ]
                )

            let leaves = bigObligations.map(calculateLeafFromObligation)
            //test different sizes of merkle tree generation
            for (let ii = 1; ii < leaves.length; ii++) {
                // get merkle roots to compare
                ethersMerkleRoot = await merkleFromObs(bigObligations.slice(0, ii))
                let contractMerkleRoot = await consensus.callStatic.getMerkleRoot(leaves.slice(0, ii))
                expect(ethersMerkleRoot).to.be.eq(contractMerkleRoot)
                // check that the verify function is working. It accepts obligations arrays and turns them to merkle leaves.
                expect(await consensus.callStatic.verifyMerkleTree(bigObligations.slice(0, ii))).to.be.eq(
                    ethersMerkleRoot
                )
            }
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Tests the Collateral Slashing Calculator", async () => {
        it("The Collateral Slashing Divider Function Works properly for whole number segments", async () => {
            let senders: CollateralTransfer[]
            senders = [
                {party: bobAddr, amount: 200}, // 200
                {party: aliceAddr, amount: 600}, // 800
                {party: charlieAddr, amount: 400}, // 1200
                {party: sdpAdmin1Address, amount: 300}, // 1500
            ]
            let recipients: CollateralTransfer[]
            recipients = [
                {party: bobAddr, amount: 100}, //100
                {party: aliceAddr, amount: 400}, //500
                {party: charlieAddr, amount: 700}, //1200
                {party: sdpAdmin1Address, amount: 100}, //1300
                {party: sdpAdmin2Address, amount: 200}, //1500
            ]

            let slashObligations: TransferObligation[]
            slashObligations = await consensus.callStatic.generateTransferObligations(senders, recipients)

            // the arrays are expected to be padded with zeros because creating and pushing to a dynamic sized array
            // in memory is not possible in solidity
            let expectedTransferAmounts = [100, 100, 300, 300, 400, 100, 200, 0, 0]
            let expectedSenders = [
                bobAddr,
                bobAddr,
                aliceAddr,
                aliceAddr,
                charlieAddr,
                sdpAdmin1Address,
                sdpAdmin1Address,
                _za,
                _za,
            ]
            let expectedRecipients = [
                bobAddr,
                aliceAddr,
                aliceAddr,
                charlieAddr,
                charlieAddr,
                sdpAdmin1Address,
                sdpAdmin2Address,
                _za,
                _za,
            ]
            for (let ii = 0; ii < expectedTransferAmounts.length; ii++) {
                expect(slashObligations[ii].sender).to.be.eq(expectedSenders[ii])
                expect(slashObligations[ii].recipient).to.be.eq(expectedRecipients[ii])
                expect(slashObligations[ii].amount).to.be.eq(expectedTransferAmounts[ii])
            }
        })

        it("The Collateral Slashing Divider Function Works properly for Rounding Errors", async () => {
            let senders: CollateralTransfer[]
            senders = [
                {party: bobAddr, amount: 199},
                {party: aliceAddr, amount: 599},
                {party: charlieAddr, amount: 399},
                {party: sdpAdmin1Address, amount: 299},
            ]
            let recipients: CollateralTransfer[]
            recipients = [
                {party: bobAddr, amount: 99},
                {party: aliceAddr, amount: 399},
                {party: charlieAddr, amount: 699},
                {party: sdpAdmin1Address, amount: 99},
                {party: sdpAdmin2Address, amount: 199},
            ]
            let slashObligations: TransferObligation[]
            slashObligations = await consensus.callStatic.generateTransferObligations(senders, recipients)

            // the arrays are expected to be padded with zeros because creating and pushing to a dynamic sized array
            // in memory is not possible in solidity

            let expectedTransferAmounts = [100, 100, 300, 300, 400, 100, 200, 0, 0]
            let expectedSenders = [
                bobAddr,
                bobAddr,
                aliceAddr,
                aliceAddr,
                charlieAddr,
                sdpAdmin1Address,
                sdpAdmin1Address,
                _za,
                _za,
            ]
            let expectedRecipients = [
                bobAddr,
                aliceAddr,
                aliceAddr,
                charlieAddr,
                charlieAddr,
                sdpAdmin1Address,
                sdpAdmin2Address,
                _za,
                _za,
            ]
            for (let ii = 0; ii < expectedTransferAmounts.length; ii++) {
                expect(slashObligations[ii].sender).to.be.eq(expectedSenders[ii])
                expect(slashObligations[ii].recipient).to.be.eq(expectedRecipients[ii])
                expect(slashObligations[ii].amount.toNumber()).to.be.greaterThan(
                    expectedTransferAmounts[ii] - expectedTransferAmounts.length
                )
                expect(slashObligations[ii].amount.toNumber()).to.be.lessThanOrEqual(
                    expectedTransferAmounts[ii]
                )
            }
        })
    })
    describe("Tests the Obligation Comparing Algorighthm", async () => {
        beforeEach(async () => {
            // fakeObligations = bigObligations.slice(0, bigObligations.length - 1)
        })
        it("Algorithm can account for a missing obligation", async () => {
            fakeObligations = bigObligations.slice(0, bigObligations.length - 1)
            let transferObs: CollateralTransfer[]
            transferObs = await consensus.callStatic.compareObligationAmounts(bigObligations, fakeObligations)
            for (let ii = 0; ii < transferObs.length; ii++) {
                if (transferObs[ii].party == bigObligations[bigObligations.length - 1].recipient) {
                    await expect(transferObs[ii].amount).to.be.eq(
                        bigObligations[bigObligations.length - 1].amount
                    )
                } else {
                    await expect(transferObs[ii].amount).to.be.eq(0)
                }
            }
        })

        it("Algorithm can account for multiple overreports and reward deliverer", async () => {
            fakeObligations = []
            bigObligations.forEach(val => fakeObligations.push(Object.assign({}, val)))

            //alice over pays by 2000 in two obs for a total of 4000
            fakeObligations[3].amount = 3000
            fakeObligations[4].amount = 4000
            let transferObs: CollateralTransfer[]
            transferObs = await consensus.callStatic.compareObligationAmounts(bigObligations, fakeObligations)

            for (let ii = 0; ii < transferObs.length; ii++) {
                if (transferObs[ii].party == aliceAddr) {
                    await expect(transferObs[ii].amount).to.be.eq(4000)
                } else {
                    await expect(transferObs[ii].amount).to.be.eq(0)
                }
            }
        })

        it("Algorithm can account for multiple underreports and reward recipient", async () => {
            fakeObligations = []
            bigObligations.forEach(val => fakeObligations.push(Object.assign({}, val)))

            //bob under recieved by 1000 in two obs for a total of 2000
            fakeObligations[1].amount = 1000
            fakeObligations[6].amount = 1000
            let transferObs: CollateralTransfer[]
            transferObs = await consensus.callStatic.compareObligationAmounts(bigObligations, fakeObligations)
            for (let ii = 0; ii < transferObs.length; ii++) {
                if (transferObs[ii].party == bobAddr) {
                    const expectedAmount = transferObs[ii].party == bobAddr ? 2000 : 0
                    await expect(transferObs[ii].amount).to.be.eq(expectedAmount)
                }
            }
        })

        it("Algorithm can account for many missing obligations and sums up all losses and gains from missreport", async () => {
            // results in multiple underreports and overreports for multiple users

            //This test tests a combination of net gains and losses from underdelivery, overdelivery, underreciept, and overreciept

            fakeObligations = bigObligations.slice(0, 0)

            let transferObs: CollateralTransfer[]
            transferObs = await consensus.callStatic.compareObligationAmounts(bigObligations, fakeObligations)

            await expect(transferObs[0].amount).to.be.eq(11000)
            await expect(transferObs[0].party).to.be.eq(bobAddr)
            await expect(transferObs[1].amount).to.be.eq(0)
            await expect(transferObs[1].party).to.be.eq(aliceAddr)
            await expect(transferObs[2].amount).to.be.eq(2000)
            await expect(transferObs[2].party).to.be.eq(charlieAddr)
            await expect(transferObs[3].amount).to.be.eq(0)
            await expect(transferObs[3].party).to.be.eq(sdpAdmin1Address)
        })
    })
})
