import { Signer } from "ethers"
import { ethers, getNamedAccounts, getUnnamedAccounts } from "hardhat"
import hre from "hardhat"
import { depositToken } from "../token-deposit"
import { setupTest, AssetCustody, DummyCoin } from "./utils"
import { expect } from "chai"
import { Address } from "hardhat-deploy/types"

describe("Tasks", function () {
  let assetCustody: AssetCustody
  let dummyCoin: DummyCoin
  let alice: Address
  let aliceSigner: Signer

  before(async () => {
    const namedAccounts = await getNamedAccounts()
    aliceSigner = ethers.provider.getSigner(namedAccounts.alice)
    alice = namedAccounts.alice
  })

  beforeEach(async function () {
    const contracts = await setupTest()
    assetCustody = contracts.assetCustody
    dummyCoin = contracts.dummyCoin
  })

  it("deposits DummyCoin to named account", async () => {
    const amount = 1000

    const aliceBalanceBefore = await dummyCoin.callStatic.balanceOf(alice)
    const aliceDepositBalanceBefore =
      await assetCustody.callStatic.depositBalances(alice, dummyCoin.address)

    await depositToken("alice", amount, hre, "DummyCoin")

    const aliceBalanceAfter = await dummyCoin.callStatic.balanceOf(alice)
    const aliceDepositBalanceAfter =
      await assetCustody.callStatic.depositBalances(alice, dummyCoin.address)

    expect(aliceBalanceBefore.sub(amount)).to.be.eq(aliceBalanceAfter)
    expect(aliceDepositBalanceBefore.add(amount)).to.be.eq(
      aliceDepositBalanceAfter
    )
  })

  it("deposits DummyCoin to unnamed account", async () => {
    const account = (await getUnnamedAccounts())[29]
    const amount = 1000
    await dummyCoin.mint(account, amount)

    const balanceBefore = await dummyCoin.callStatic.balanceOf(account)
    const depositBalanceBefore = await assetCustody.callStatic.depositBalances(
      account,
      dummyCoin.address
    )

    await depositToken(account, amount, hre, "DummyCoin")

    const balanceAfter = await dummyCoin.callStatic.balanceOf(account)
    const depositBalanceAfter = await assetCustody.callStatic.depositBalances(
      account,
      dummyCoin.address
    )

    expect(balanceBefore.sub(amount)).to.be.eq(balanceAfter)
    expect(depositBalanceBefore.add(amount)).to.be.eq(depositBalanceAfter)
  })
})
