# txa-settlement-contracts

Copyright © 2022 TXA PTE. LTD.

### Brief

Solidity contracts for on-chain collateral and settlement.

Please refer to comments of each individual contract for detailed explanations of functionality.

### Getting started

After cloning:

```
nvm use lts/fermium
npm i
npm run compile
npm run ganache-eth
```

In a separate terminal:

```
npm run deploy test_eth
```

This should deploy all contracts to the local running ganache instance.

After deployment the relevant contract addresses can be found in `./deployments.`

### Run Unit Tests
Current as of Apr 27 2022

```
nvm use lts/gallium
npm i
npx hardhat compile
npx hardhat test/unit-tests/*ts
```

### Test Chain

Run `npm run ganache-eth` to run the basic eth test chain.

### Simulating Settlement

To fully simulate a settlement of the native asset and a test ERC20, from deposit to recollateralizing,
first start an instance of the docker image with contracts pre-deployed (registry.gitlab.com/projecttxa/txa-dsl/txa-settlement-contracts/txa-settlement-contracts-cliquebait:0.8) and then run the following commands:
```
 npx hardhat run --network cliquebait scripts/deposit_eth.ts &&
 npx hardhat run --network cliquebait scripts/request_settlement.ts &&
 npx hardhat run --network cliquebait scripts/report_obligations.ts &&
 npx hardhat run --network cliquebait scripts/request_funds_owed.ts &&
 npx hardhat run --network cliquebait scripts/collateralize.ts
 ```

### Linter

`npm run solhint`

### Prettier

`npm run format`

### Compile

`npm run compile`

### Test

`npm run test`.

### Code Coverage Report

`npm run coverage`

### License

This software is distributed under the Modified BSD License (3-Clause BSD License), see LICENSE for more information.
