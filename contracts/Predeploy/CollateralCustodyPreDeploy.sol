// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "./PredeployConstants.sol";
import "../CollateralCustody.sol";

contract CollateralCustodyPreDeploy is CollateralCustody, PredeployConstants {
    constructor() CollateralCustody(IDENTITY_REGISTRY) {}

    function initialize() external {
        identity = IdentityRegistryInterface(IDENTITY_REGISTRY);
        approve(SDP_OPERATOR, SDP_ADMIN);
    }

    function approve(address operator, address admin) public {
        adminFor[operator] = admin;
        emit OperatorApproved(operator, msg.sender);
    }
}
