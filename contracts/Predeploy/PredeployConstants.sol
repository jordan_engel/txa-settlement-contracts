// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

/**
 * Defines addresses where we set the corresponding contract bytecode in geth genesis block.
 * Also hardcodes addresses that will receive funds in genesis block and permissions in system contracts.
 */
contract PredeployConstants {
    address public constant IDENTITY_REGISTRY = 0x18e5b9d018cAb60fC73A79D560EA61dd856a808d;
    address public constant CONSENSUS = 0x7aa2a80e4806754Db209eeAA3F82A050F63eC45A;
    address public constant COORDINATOR = 0x41Ae37E15B1D8F1d525B513467F3523E4cA31120;
    address public constant SINGLETON = 0x8A3cB37a7aC1acEd79034E93D409906eACb2Da6b;
    address public constant COLLATERAL_CUSTODY = 0x74B295E773fc218f2a43D1569116cBE98Bef5981;
    address public constant LIBRARY = 0xD91e1365a352e6542A2d742577F1375ADCa84711;
    address public constant DUMMY_COIN = 0xa85233C63b9Ee964Add6F2cffe00Fd84eb32338f;
    address public constant USDT_COIN = 0xdAC17F958D2ee523a2206206994597C13D831ec7;
    // Should match address set as 'governance' in hardhat.config.ts
    address public constant GOVERNANCE = 0x564a072Ea756b305ED265405D3eF162ef4d45085;
    // Should match address set as 'feeRecipient' in hardhat.config.ts
    address public constant FEE_RECIPIENT = 0x085DA700E41C5f24167A3e5266A60D1Ea32d10F4;
    address public constant ALICE = 0xEE214aC2A18bCDf7F563f63a2Cbc30DdAD9DB2E4;
    address public constant BOB = 0x537B304C9B632FFE222C222CD455d2bC6ae6af1f;
    address public constant SDP_ADMIN = 0x6C07bB87a8fB71fB828856c38affc7aEC2A7cc89;
    address public constant SDP_OPERATOR = 0x8398c90E73daa892b48FaFe362AE15927a5CB631;
}
