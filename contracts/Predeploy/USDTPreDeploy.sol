// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "./PredeployConstants.sol";
import "../Helpers/USDT.sol";

contract USDTPreDeploy is USDT, PredeployConstants {
    constructor(address[] memory airdrop) USDT(airdrop, "Tether USD", "USDT") {}

    function initialize() external {
        _mint(ALICE, 100000000000000000000);
        _mint(BOB, 100000000000000000000);
    }

    function name() public view override returns (string memory) {
        return "Tether USD";
    }

    function symbol() public view override returns (string memory) {
        return "USDT";
    }
}
