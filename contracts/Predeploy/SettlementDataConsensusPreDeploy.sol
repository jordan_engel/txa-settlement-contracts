// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "./PredeployConstants.sol";
import "../SettlementDataConsensus.sol";

contract SettlementDataConsensusPreDeploy is SettlementDataConsensus, PredeployConstants {
    constructor() SettlementDataConsensus(IDENTITY_REGISTRY) {}

    function initialize() external {
        minQuorum = 1;
        identity = IdentityRegistryInterface(IDENTITY_REGISTRY);
        minDifficulty = 0;
        maxDifficulty = 0;
    }
}
