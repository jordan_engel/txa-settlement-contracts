// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "./PredeployConstants.sol";
import "../Helpers/DummyCoin.sol";

contract DummyCoinPreDeploy is DummyCoin, PredeployConstants {
    constructor(address[] memory airdrop) DummyCoin(airdrop, "DummyCoin", "DMC") {}

    function initialize() external {
        _mint(ALICE, 100000000000000000000);
        _mint(BOB, 100000000000000000000);
        _mint(SDP_ADMIN, 100000000000000000000);
    }

    function name() public view override returns (string memory) {
        return "DummyCoin";
    }

    function symbol() public view override returns (string memory) {
        return "DMC";
    }
}
