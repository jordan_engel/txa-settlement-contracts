// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "../CollateralCustody.sol";

/**
 * Provides a mocked version of the CollateralCustody to call when
 * unit testing SettlementDataConsensus.
 */
contract CollateralMock is CollateralCustody {
    constructor(address _identity) CollateralCustody(_identity) {}

    /**
     * Overrides approveOperator to immediately approve the sender
     * as the admin of operator without verifying the signature.
     */
    function approveOperator(
        address operator,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external override {
        adminFor[operator] = msg.sender;
    }

    /**
     * Overrides revokeOperator to immediately reset the admin of
     * operator without any auth checks.
     */
    function revokeOperator(address operator) external override {
        delete adminFor[operator];
    }
}
