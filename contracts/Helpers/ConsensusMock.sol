// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "../SettlementDataConsensus.sol";

contract ConsensusMock is SettlementDataConsensus {
    constructor(address _identity) SettlementDataConsensus(_identity) {
        // below effectively overrides the proof-of-work mechanism
        maxBlocks = 0;
        minDifficulty = 0;
        maxDifficulty = 0;
    }

    function callLockCollateral(
        address account,
        address token,
        uint256 amount,
        address collateralAddress
    ) external {
        CollateralInterface(collateralAddress).lockCollateral(account, token, amount);
    }

    function callUnlockCollateral(
        address account,
        address token,
        uint256 amount,
        address collateralAddress
    ) external {
        CollateralInterface(collateralAddress).unlockCollateral(account, token, amount);
    }

    function callSlashCollateralForUser(
        address deliverer,
        address recipient,
        address token,
        uint256 amount,
        address collateralAddress
    ) external {
        CollateralInterface(collateralAddress).slashCollateralForUser(deliverer, recipient, token, amount);
    }

    function callSlashCollateral(
        address deliverer,
        address recipient,
        address token,
        uint256 amount,
        address collateralAddress
    ) external {
        CollateralInterface(collateralAddress).slashCollateral(deliverer, recipient, token, amount);
    }

    function getDisputeObligations(bytes32 obligationsId, address reporter)
        external
        returns (SettlementLib.Obligation[] memory)
    {
        return settlements[obligationsId].disputeObligations[reporter];
    }
}
