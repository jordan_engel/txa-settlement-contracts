// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "./Interfaces/IdentityRegistryInterface.sol";
import "./Interfaces/CollateralInterface.sol";

/**
 */

contract MerkleState {
    /**
     * Instance of IdentityRegistry interface.
     */
    IdentityRegistryInterface internal identity;

    mapping(uint256 => bytes32) public tradeRoots;

    uint256 public tradeRootID = 0;

    constructor(address _identity) {
        identity = IdentityRegistryInterface(_identity);
    }

    function reportTradeRoot(bytes32 tradeRoot) public {
        require(
            CollateralInterface(identity.getLatestCollateralCustody()).adminFor(msg.sender) != address(0),
            "NOT_APPROVED_OPERATOR"
        );

        tradeRoots[tradeRootID] = tradeRoot;
        tradeRootID++;
    }
}
