// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

/**
 * ProofOfWork provides a mechanism for requiring SDPs to submit a nonce alongside their reported obligations.
 * This nonce is hashed with parameters that are either unique to the reporter or the settlement itself.
 * In order for the nonce to be accepted, a number of the least significant bits of the hash must be 0.
 * The number of bits that must be 0 is determined by the difficulty, which decreases as time goes on.
 */
contract ProofOfWork {
    /**
     * The number of blocks that must pass since a settlement is requested
     * for the difficulty to reach its minimum requirement.
     */
    uint256 public maxBlocks;

    /**
     * The minimum number of least significant bits that must be 0 for a hash to be valid.
     */
    uint256 public minDifficulty = 2;

    /**
     * The maximum number of least significant bits that must be 0 for a hash to be valid.
     */
    uint256 public maxDifficulty = 64;

    constructor(uint256 _maxBlocks) {
        maxBlocks = _maxBlocks;
    }

    /**
     * Given a nonce and settlement parameters, determines if nonce is valid.
     *
     * Calculates the work value by hashing the nonce with settlement parameters.
     * Determines difficulty for the settlement based on the block number when the settlement was requested.
     * Validates that the work meets the requirements of the current difficulty for the settlement.
     *
     * @param nonce Nonce submitted by the SDP operator attempting to report for a settlement
     * @param merkleRoot Merkle root of obligations submitted by the SDP operator for the settlement
     * @param reporter Address of the SDP operator reporting obligations
     * @param settlementId ID of the settlement
     * @param lastReporter The address of the final SDP operator that reported for the previous settlement.
     * @param startBlock Block number when the settlement was requested
     */
    function validateNonce(
        uint256 nonce,
        bytes32 merkleRoot,
        address reporter,
        uint256 settlementId,
        address lastReporter,
        uint256 startBlock
    ) public view returns (bool) {
        bytes32 work = keccak256(abi.encode(nonce, merkleRoot, reporter, settlementId, lastReporter));
        return validateWork(work, calculateDifficulty(startBlock));
    }

    /**
     * Given a block number of when a settlement was requested, determines the difficulty
     * of work that must be completed in order to participate in the settlement.
     *
     * The difficulty is the number of least significant bits of the work value
     * that must be 0. As time goes on, the difficulty decreases.
     * If the maximum amount of blocks have elapsed, then the minimum difficulty is returned.
     * Otherwise, it's calculated based on the ratio of how many blocks have passsed and the
     * maximum amount of blocks. That ratio is applied to the difference between the maximum
     * and minimum difficulty, resulting in a difficulty between the two limits.
     *
     * @param startBlock Block number when a settlement was requested
     */
    function calculateDifficulty(uint256 startBlock) public view returns (uint256) {
        uint256 blocksElapsed = block.number - startBlock;
        if (blocksElapsed >= maxBlocks) {
            return minDifficulty;
        }
        uint256 delta = ((maxDifficulty - minDifficulty) * blocksElapsed) / maxBlocks;
        return maxDifficulty - delta;
    }

    /**
     * Validates that the least significant bits of submitted work are all 0.
     *
     * The number of least significant bits that must be 0 is determined
     * by the difficulty parameter. The higher the difficulty, the more
     * bits that must be zero.
     * e.g. with a difficulty of 1, only the very last bit of the nonce
     * must be 0, because the nonce will be left-shifted by 255 bits, setting
     * every bit except the last one to 0 before comparing.
     * With a difficulty of 63, every single bit except the first
     *
     * @param work The calculated hash submitted as work
     * @param difficulty How many least significant bits of the work must be 0
     */
    function validateWork(bytes32 work, uint256 difficulty) public view returns (bool) {
        require(difficulty >= minDifficulty && difficulty <= maxDifficulty, "INVALID_DIFFICULTY");
        return (work << (256 - difficulty)) == 0;
    }
}
