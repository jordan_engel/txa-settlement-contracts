// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

contract ExchangeEIP712 {
    struct EIP712Domain {
        string name;
        string version;
        uint256 chainId;
        address verifyingContract;
        bytes32 salt;
    }

    struct Identity {
        string baseAssetEscrow;
        string baseAssetNetworkType;
        string baseAssetChainId;
        string counterAssetEscrow;
        string counterAssetNetworkType;
        string counterAssetChainId;
    }

    struct Order {
        Identity identity;
        string product;
        string side;
        string orderType;
        string fillType;
        string size;
        string price;
    }

    bytes32 public constant salt = 0xf2d857f4a3edcb9b78b4d503bfe733db1e3f6cdc2b7971ee739626c97e86a558;
    string public constant domainName = "TXA";

    bytes32 private constant EIP712DOMAIN_TYPEHASH =
        keccak256(
            abi.encodePacked(
                "EIP712Domain(string name,string version,uint256 chainId,address verifyingContract,bytes32 salt)"
            )
        );
    bytes32 private constant IDENTITY_TYPEHASH =
        keccak256(
            abi.encodePacked(
                "Identity(string baseAssetEscrow,string baseAssetNetworkType,string baseAssetChainId,string counterAssetEscrow,string counterAssetNetworkType,string counterAssetChainId)"
            )
        );
    bytes32 private constant ORDER_TYPEHASH =
        keccak256(
            abi.encodePacked(
                "Order(Identity identity,string product,string side,string orderType,string fillType,string size,string price)",
                "Identity(string baseAssetEscrow,string baseAssetNetworkType,string baseAssetChainId,string counterAssetEscrow,string counterAssetNetworkType,string counterAssetChainId)"
            )
        );
    bytes32 private DOMAIN_SEPARATOR;

    constructor(string memory version, uint256 chainId) public {
        DOMAIN_SEPARATOR = hash(
            EIP712Domain({
                name: domainName,
                version: version,
                chainId: chainId,
                verifyingContract: address(this),
                salt: salt
            })
        );
    }

    function hash(EIP712Domain memory eip712Domain) internal view returns (bytes32) {
        return
            keccak256(
                abi.encode(
                    EIP712DOMAIN_TYPEHASH,
                    keccak256(bytes(eip712Domain.name)),
                    keccak256(bytes(eip712Domain.version)),
                    eip712Domain.chainId,
                    address(this),
                    eip712Domain.salt
                )
            );
    }

    function hashIdentity(Identity memory identity) private pure returns (bytes32) {
        return
            keccak256(
                abi.encode(
                    IDENTITY_TYPEHASH,
                    keccak256(bytes(identity.baseAssetEscrow)),
                    keccak256(bytes(identity.baseAssetNetworkType)),
                    keccak256(bytes(identity.baseAssetChainId)),
                    keccak256(bytes(identity.counterAssetEscrow)),
                    keccak256(bytes(identity.counterAssetNetworkType)),
                    keccak256(bytes(identity.counterAssetChainId))
                )
            );
    }

    function hashOrder(Order memory order) private view returns (bytes32) {
        return
            keccak256(
                abi.encodePacked(
                    "\x19\x01",
                    DOMAIN_SEPARATOR,
                    keccak256(
                        abi.encode(
                            ORDER_TYPEHASH,
                            hashIdentity(order.identity),
                            keccak256(bytes(order.product)),
                            keccak256(bytes(order.side)),
                            keccak256(bytes(order.orderType)),
                            keccak256(bytes(order.fillType)),
                            keccak256(bytes(order.size)),
                            keccak256(bytes(order.price))
                        )
                    )
                )
            );
    }

    function verify(
        address signer,
        Order memory order,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) public view returns (bool) {
        return ecrecover(hashOrder(order), v, r, s) == signer;
    }
}
