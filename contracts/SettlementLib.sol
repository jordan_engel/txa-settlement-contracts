// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

/**
 * Library for shared constants and data structures.
 */
library SettlementLib {
    /**
     * Role assigned to the governance address
     */
    bytes32 public constant ROLE_GOVERNANCE = keccak256("ROLE_GOVERNANCE");

    /**
     * Role assigned to the SettlementCoordination contract
     */
    bytes32 public constant ROLE_LOCALCOORD = keccak256("ROLE_LOCALCOORD");

    /**
     * Role assigned to the AssetCustody contract
     */
    bytes32 public constant ROLE_ASSET_CUSTODY = keccak256("ROLE_ASSET_CUSTODY");

    /**
     * Role assigned to the CollateralCustody contract
     */
    bytes32 public constant ROLE_COLLATERAL_CUSTODY = keccak256("ROLE_COLLATERAL_CUSTODY");

    /**
     * Role assigned to the SettlementDataConsensus contract
     */
    bytes32 public constant ROLE_CONSENSUS = keccak256("ROLE_CONSENSUS");

    /**
     * Role assigned to the SDP administrators
     */
    bytes32 public constant ROLE_SDP_ADMIN = keccak256("ROLE_SDP_ADMIN");

    /**
     * Role assigned to auditors
     */
    bytes32 public constant ROLE_AUDITOR = keccak256("ROLE_AUDITOR");

    /**
     * Role assigned to tokens eligible for deposit as collateral for both
     * traders and SDPs.
     */
    bytes32 public constant TRADEABLE_TOKEN = keccak256("TRADEABLE_TOKEN");

    /**
     * Role assigned to token that SDPs must stake as additional collateral
     * in order to participate in settlement.
     */
    bytes32 public constant PROTOCOL_TOKEN = keccak256("PROTOCOL_TOKEN");

    /**
     * Stores all information necessary to settle an asset between traders.
     *
     * Each settlement will result in the reporting of one or more obligations.
     * Each obligation tells the AssetCustody contract which party needs to allocate
     * to another counterparty a specified amount of a single asset.
     */
    struct Obligation {
        uint256 amount;
        address deliverer;
        address recipient;
        address token;
        bool reallocate;
    }
}
