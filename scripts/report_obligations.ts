import { ethers, getNamedAccounts, deployments } from "hardhat"
import { merkleFromObs, Obligation } from "../test/utils"
import { DummyCoin } from "../typechain/DummyCoin"
import { SettlementCoordination } from "../typechain/SettlementCoordination"
import { SettlementDataConsensus } from "../typechain/SettlementDataConsensus"
import { CollateralCustodyPreDeploy } from "../typechain/CollateralCustodyPreDeploy"

/**
 * Reports obligations of DummyCoin owed to Alice
 * and native asset owed to Bob by sending transactions
 * to the predeployed Consensus contract on behalf of
 * SDP operators.
 *
 * MUST run request_settlement.ts before this script
 *
 * This script is used to simulate state of a settlement
 * when testing the front-end. It expects Alice to have
 * requested settlement for DummyCoin and Bob to have
 * requested settlement for native asset.
 * Assumes using predeployed contracts with a quorum of 1.
 */
export async function reportObligations() {
  const { alice, bob, sdpOperator1, sdpAdmin1 } = await getNamedAccounts()
  const consensus = (await ethers.getContractAt(
    "SettlementDataConsensus",
    "0x7aa2a80e4806754Db209eeAA3F82A050F63eC45A"
  )) as SettlementDataConsensus
  const dummyCoin = (await ethers.getContractAt(
    "DummyCoin",
    "0xa85233C63b9Ee964Add6F2cffe00Fd84eb32338f"
  )) as DummyCoin
  const coordinator = (await ethers.getContractAt(
    "SettlementCoordination",
    "0x41Ae37E15B1D8F1d525B513467F3523E4cA31120"
  )) as SettlementCoordination
  const collateral = (await ethers.getContractAt(
    "CollateralCustodyPreDeploy",
    "0x74B295E773fc218f2a43D1569116cBE98Bef5981"
  )) as CollateralCustodyPreDeploy

  if (
    (await dummyCoin.callStatic.allowance(sdpAdmin1, collateral.address)).lt(
      ethers.utils.parseEther("1.256").mul(4)
    )
  ) {
    await (
      await dummyCoin
        .connect(await ethers.getSigner(sdpAdmin1))
        .approve(collateral.address, ethers.constants.MaxUint256)
    ).wait()
  }
  if (
    (
      await collateral.callStatic.getAvailableBalance(
        sdpAdmin1,
        dummyCoin.address
      )
    ).lt(ethers.utils.parseEther("1.256").mul(2))
  ) {
    await collateral
      .connect(await ethers.getSigner(sdpAdmin1))
      .callStatic.depositToken(
        ethers.utils.parseEther("1.256").mul(4),
        dummyCoin.address
      )
    await (
      await collateral
        .connect(await ethers.getSigner(sdpAdmin1))
        .depositToken(
          ethers.utils.parseEther("1.256").mul(4),
          dummyCoin.address
        )
    ).wait()
  }
  if (
    (
      await collateral.callStatic.getAvailableBalance(
        sdpAdmin1,
        ethers.constants.AddressZero
      )
    ).lt(ethers.utils.parseEther("1.256").mul(2))
  ) {
    await collateral
      .connect(await ethers.getSigner(sdpAdmin1))
      .callStatic.deposit({ value: ethers.utils.parseEther("1.256").mul(4) })
    await (
      await collateral
        .connect(await ethers.getSigner(sdpAdmin1))
        .deposit({ value: ethers.utils.parseEther("1.256").mul(4) })
    ).wait()
  }

  const nextRequestId = await coordinator.callStatic.nextRequestId()
  let lastId = await coordinator.callStatic.lastSettlementIdProcessed()
  while (lastId.lt(nextRequestId.sub(1))) {
    const reqData = await coordinator.callStatic.getReqData(lastId.add(1))
    let deliverer
    let recipient
    if (reqData.token == dummyCoin.address) {
      // Report obligation where Bob owes Alice DummyCoin
      deliverer = bob
      recipient = alice
    } else if (reqData.token == ethers.constants.AddressZero) {
      // Report obligation where Alice owes Bob native asset
      deliverer = alice
      recipient = bob
    } else {
      throw Error("Not expecting token besides DummyCoin or address zero")
    }
    const obligation: Obligation = {
      amount: ethers.utils.parseEther("1.256"),
      deliverer,
      recipient,
      reallocate: false,
      token: reqData.token,
    }
    const merkleRoot = await merkleFromObs([obligation])
    await consensus
      .connect(await ethers.getSigner(sdpOperator1))
      .callStatic.reportSettlementObligations(
        coordinator.address,
        lastId.add(1),
        [obligation],
        merkleRoot,
        0
      )
    const tx = await (
      await consensus
        .connect(await ethers.getSigner(sdpOperator1))
        .reportSettlementObligations(
          coordinator.address,
          lastId.add(1),
          [obligation],
          merkleRoot,
          0
        )
    ).wait()
    console.log("Reported obligations for settlement with")
    console.log("ID: " + lastId.add(1).toNumber())
    console.log("Token: " + reqData.token)
    console.log("Requestor: " + recipient)
    console.log("Call requestFundsOwed with counterparty: " + deliverer)

    lastId = await coordinator.callStatic.lastSettlementIdProcessed()
  }
}

reportObligations()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error)
    process.exit(1)
  })
