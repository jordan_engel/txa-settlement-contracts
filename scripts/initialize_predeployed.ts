import { ethers } from "hardhat"
import { IdentityRegistryPreDeploy } from "../typechain/IdentityRegistryPreDeploy"

/**
 * Calls the identity registry on geth test chain and initializes the system contracts.
 *
 * We do this because our custom genesis block only sets the bytecode, not the storage of each contract.
 * This script calls the function on IdentityRegistry that in turn calls each function on the
 * system contracts that replicate the behavior of each constructor.
 *
 * After this script is run, the test chain reaches a state that is ready for integration tests
 * with SDP, indexer, etc.
 */
export async function initializePredeployedContracts() {
  const identity = (await ethers.getContractAt(
    "IdentityRegistryPreDeploy",
    "0x18e5b9d018cAb60fC73A79D560EA61dd856a808d"
  )) as IdentityRegistryPreDeploy
  await (await identity.initialize(241)).wait()
  console.log("initialized predeployed contracts")
}

initializePredeployedContracts()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error)
    process.exit(1)
  })
