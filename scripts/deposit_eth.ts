import { ethers, getNamedAccounts, deployments } from "hardhat"
import { AssetCustody } from "../typechain/AssetCustody"
import { DummyCoin } from "../typechain/DummyCoin"

export async function depositEth() {
  const { alice, bob, exchange } = await getNamedAccounts()
  const dummyCoin = (await ethers.getContractAt(
    "DummyCoin",
    "0xa85233C63b9Ee964Add6F2cffe00Fd84eb32338f"
  )) as DummyCoin
  const assetCustody = (await ethers.getContractAt(
    "AssetCustody",
    "0x8A3cB37a7aC1acEd79034E93D409906eACb2Da6b"
  )) as AssetCustody
  for (const address of [alice, bob]) {
    await (
      await assetCustody
        .connect(await ethers.getSigner(address))
        .deposit(false, { value: ethers.utils.parseEther("1.256") })
    ).wait()
    await (
      await dummyCoin
        .connect(await ethers.getSigner(address))
        .approve(assetCustody.address, ethers.utils.parseEther("1.256"))
    ).wait()
    await (
      await assetCustody
        .connect(await ethers.getSigner(address))
        .depositToken(
          ethers.utils.parseEther("1.256"),
          dummyCoin.address,
          false
        )
    ).wait()
  }
  console.log("deposited 1.256 ETH and DMC for alice and bob")
}

depositEth()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error)
    process.exit(1)
  })
