import { ethers, getNamedAccounts, deployments } from "hardhat"
import { AssetCustody } from "../typechain/AssetCustody"
import { DummyCoin } from "../typechain/DummyCoin"

/**
 * Collateralizes DummyCoin on behalf of Alice
 * and the native asset on behalf of Bob
 * by sending transactions to the predeployed AssetCustody
 * contract.
 *
 * MUST run report_obligations.ts before this script
 *
 * Expects SDPs to have reported for Alice and Bob's settlements.
 */
export async function collateralize() {
  const { alice, bob } = await getNamedAccounts()
  const dummyCoin = (await ethers.getContractAt(
    "DummyCoin",
    "0xa85233C63b9Ee964Add6F2cffe00Fd84eb32338f"
  )) as DummyCoin
  const assetCustody = (await ethers.getContractAt(
    "AssetCustody",
    "0x8A3cB37a7aC1acEd79034E93D409906eACb2Da6b"
  )) as AssetCustody

  for (const [address, token] of [
    [alice, dummyCoin.address],
    [bob, ethers.constants.AddressZero],
  ]) {
    console.log(address)
    console.log("collateralizing for " + token)

    await assetCustody
      .connect(await ethers.getSigner(address))
      .callStatic.collateralize(token)
    await (
      await assetCustody
        .connect(await ethers.getSigner(address))
        .collateralize(token)
    ).wait()
  }
  console.log("Alice collateralized for DMC " + dummyCoin.address)
  console.log(
    "Bob collateralized for native asset " + ethers.constants.AddressZero
  )
}

collateralize()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error)
    process.exit(1)
  })
